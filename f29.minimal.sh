#!/bin/bash

#
# Generate minimal container image (~57MB) from Fedora:29 repo using buildah (https://github.com/projectatomic/buildah)
#

set -ex

# start new container from scratch
newcontainer=$(buildah --storage-driver vfs from scratch)
scratchmnt=$(buildah --storage-driver vfs mount ${newcontainer})

# install the packages
dnf install --installroot ${scratchmnt} bash coreutils --releasever 29 --setopt=tsflags=nodocs --setopt=override_install_langs=en_US.utf8 -y

# Clean up yum cache
if [ -d "${scratchmnt}" ]; then
  rm -rf "${scratchmnt}"/var/cache/dnf
fi

# configure container label and entrypoint
buildah --storage-driver vfs config --label name=f29-minimal ${newcontainer}
buildah --storage-driver vfs config --cmd /bin/bash ${newcontainer}

# commit the image
buildah --storage-driver vfs unmount ${newcontainer}
buildah --storage-driver vfs commit ${newcontainer} f29-minimal